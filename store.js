import { observable, toJS, action } from 'mobx'
import 'isomorphic-fetch'
import * as clampy from '@clampy-js/clampy'

let store = false

export const apiUrl = 'https://api.socialistrevolution.org'

class Store {
  constructor() {
    this.site = 'https://socialistrevolution.org'
    this.apiUrl = 'https://api.socialistrevolution.org'
  }

  getCategories = async () => {
    const res = await fetch(`${apiUrl}/categories`)
    const result = await res.json()
    return result
  }

  getIndexPosts = async (isServer, page) => {
    let url = `${apiUrl}/posts?status=publish&page=${page}`
    const [countRes, postsRes] = await Promise.all([
      fetch(`${url}&count=true`),
      fetch(url),
    ])
    const [count, posts] = await Promise.all([countRes.json(), postsRes.json()])
    count.pagesLeft = Math.ceil(count.postsLeft / 12)
    count.page = page
    let result = {
      posts: posts,
      count: count,
    }
    return result
  }

  getSearchPosts = async (isServer, page, cat) => {
    //console.log(`getSearchPosts category: ${cat}`)
    // if (!isServer && cat === undefined) {
    //   let path = window.location.pathname
    //   if (path.includes('/search/')) {
    //     console.log(`client, grabbing cat from url ${path}`)
    //     const segments = path.split('/')
    //     cat = segments[segments.length - 1]
    //   }
    // }
    let url = `${apiUrl}/posts?status=publish&page=${page}`
    if (cat) url = `${url}&category=${cat}`
    //console.log(`url ${url}`)
    const [countRes, postsRes] = await Promise.all([
      fetch(`${url}&count=true`),
      fetch(url),
    ])
    const [count, posts] = await Promise.all([countRes.json(), postsRes.json()])
    count.pagesLeft = Math.ceil(count.postsLeft / 12)
    count.page = page
    //console.log(`getSearchPosts ${JSON.stringify(count)}`)
    let result = {
      posts: posts,
      count: count,
    }
    return result
  }

  clamp = () => {
    console.log('clamp')
    const contents = document.querySelectorAll('.handle-content')
    //contents.forEach(d => clampy.clamp(d, { clamp: 3 }))
  }
}

export function initStore() {
  if (store) return store
  else store = new Store()
  return store
}

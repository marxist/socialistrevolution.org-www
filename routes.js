const routes = (module.exports = require('next-routes')())

routes.add(`/search`, 'search')
routes.add(`/find/:parent/:child`, 'find')
routes.add(`/find/:parent`, 'find')
routes.add(`/find`, 'find')
routes.add('/:slug', 'post')
routes.add('/', 'index')
